import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--name", type=str, help="Nom de la personne", 
    default="world")

args = parser.parse_args()

print(f"hello {args.name}")
