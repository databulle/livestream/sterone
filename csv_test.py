import pandas as pd

df = pd.read_csv("MOCK_DATA.csv")

print(df.head())

print(
    df.groupby("first_name")["id"].count().sort_values(ascending=False)
)

print(
    df[df["gender"] == "Female"]["id"].count()
)